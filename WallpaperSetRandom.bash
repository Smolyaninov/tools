#!/bin/bash

WALLPAPERS=$"/home/nik/Wallpaper"

if [ "$1" == "-h" ] ; then
    echo "todo - Обновить только todo"    
    exit
fi

TODAY_TAG="@__today"
GOAL_TAG="@__goal"
WAIT_TAG="@__wait"


TODO_IMAGE=$WALLPAPERS/Current/todo.png
TODO_GRAVITY=SouthEast
WALLPAPER_IMAGE=$WALLPAPERS/Current/Wallpaper.jpg
WALLPAPER_TODO_IMAGE=$WALLPAPERS/Current/WallpaperTodo.jpg
WALLPAPER_LOGIN_IMAGE=/home/wallpaper/image.jpg


# wallpaper
if [ "$1" != "todo" ] ; then
    ALIST=(`ls -pw1 $WALLPAPERS | grep -v /`) # берём только файлы
    RANGE=${#ALIST[*]}
    SHOWVGA=$(( $RANDOM % $RANGE ))
    convert $WALLPAPERS/${ALIST[$SHOWVGA]} -resize 1920x1080 -background black -gravity Center -extent 1920x1080  $WALLPAPER_IMAGE
    cp $WALLPAPER_IMAGE $WALLPAPER_LOGIN_IMAGE
    chmod 644 $WALLPAPER_LOGIN_IMAGE
fi

# todo
MOTIVATION="\\
  Прокрастинация связана с неспособностью контролировать эмоции и настроение \n\\
  \n\\
  Мотивация = ( Ожидание * Ценность ) / ( Импульсивность * Задержка ) \n\\
    Ожидание - убеждение того что я смогу выполнить задачу ** Декомпозиция  \n\\
    Ценность награды ** Дополнительные плюшки: перерыв, прогулка \n\\
    Импульсивность - отвлекаемость на мгновенное удовлетворение ** Медитация, отключение инета \n\\
    Задержка - сколько время ждать, что бы получить награду"
GOALS=$(grep $GOAL_TAG /home/nik/.todo/todo.txt | grep -v h:1 | sed "s/$GOAL_TAG//" | sed -z 's/\n$//' | sed -z 's/\n/\n   /g')
TODAY=$(grep $TODAY_TAG /home/nik/.todo/todo.txt | grep -v h:1 | sed "s/$TODAY_TAG//" | sed -z 's/\n$//' | sed -z 's/\n/\n   /g')
WAIT=$(grep $WAIT_TAG /home/nik/.todo/todo.txt | grep -v h:1 | sed "s/$WAIT_TAG//" | sed -z 's/\n$//' | sed -z 's/\n/\n   /g')

TEXT_SUM="\n\\
$MOTIVATION\n\n\\
------- НА СЕГОДНЯ --------\n\\
    $TODAY\n\n\\
------- ОЖИДАЮ --------\n\\
$WAIT\n\n------- ЦЕЛИ --------\n\\
    $GOALS"

convert -size 1000x  -density 35 -pointsize 40 -background none caption:"$TEXT_SUM" $TODO_IMAGE

#todo appen to wallpaper
composite -dissolve 65 -gravity $TODO_GRAVITY -background 'rgb(200,200,200)' -alpha Remove  $TODO_IMAGE $WALLPAPER_IMAGE $WALLPAPER_TODO_IMAGE
composite -gravity $TODO_GRAVITY $TODO_IMAGE $WALLPAPER_TODO_IMAGE $WALLPAPER_TODO_IMAGE

#wallpaper set to ...
feh --bg-scale $WALLPAPER_TODO_IMAGE
#feh --bg-scale $WALLPAPER_IMAGE
