# -*- coding: utf-8 -*-

Vin = 5
R1 = 100
R2 = 4000
Vout = Vin * R2/(R1+R2)
I = Vout/R1
print ("Vout %2.2f V, I %3.0f ma" %( Vout, I*1000) )

